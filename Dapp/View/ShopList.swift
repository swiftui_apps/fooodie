//
//  ShopList.swift
//  Dapp
//
//  Created by Imthath M on 18/01/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI
import MINetworkKit
import CoreData
import SDComponents

struct ShopList: View {
    
    @State var isLoading = false
    
    let getShops = GetObject<[SDShop]>(network: Network.shared)
    @FetchRequest(fetchRequest: fetchRequest) var localShops: FetchedResults<CDShop>
    
    static var fetchRequest: NSFetchRequest<CDShop> {
        let request: NSFetchRequest<CDShop> = CDShop.fetchRequest()
        request.sortDescriptors = [NSSortDescriptor(keyPath: \CDShop.createdTime, ascending: false)]
        return request
    }
    
    init() {
        updateShops()
    }
    
    var body: some View {
        shopList
            .navigationBarTitle("Open shops")//, displayMode: .inline)
            .navigationBarItems(trailing: imageButton(withName: SDIcon.reload, andAction: updateShops))
    }
    
    var shopList: some View {
        List(localShops) { shop in
            Button(action: {
                self.showPlaces(in: shop)
            }, label: {
                ShopRow(shop: shop)
            })
                
        }
    .listStyle(GroupedListStyle())
//    .environment(\.horizontalSizeClass, .regular)
    }
    
    func showPlaces(in shop: CDShop) {
        let notification = Notification(name: .showPlaces, object: nil, userInfo: [Constant.shopKey: shop])
        NotificationCenter.default.post(notification)
    }
    
    func updateShops() {
//        showsLoader = _localShops.wrappedValue.isEmpty
        getShops.perform(Request.getShops) { result in
            self.isLoading = false
            switch result {
            case .success(let shops):
                DataManager.save(shops)
                StaticStore.lastShopSynced = UInt64(Date().timeIntervalSince1970)
            case .failure(let error):
                "\(error)".log()
            }
        }
    }
}


extension CDShop: Identifiable { }

struct ShopRow: View {
    let shop: CDShop
    
    var body: some View {
//        NavigationLink(destination: PlaceList(shop: shop), label: {
        Text(shop.name).foregroundColor(.primary)//.contextMenu(menuItems: favoriteButton)
//        })
    }
    
//    func favoriteButton() -> some View {
//        Button(action: {
//            self.shop.isFavourite.toggle()
//        }, label: {
//            if self.shop.isFavourite {
//                Text("Remove Favorite")
//            } else {
//                Text("Mark as favourite")
//            }
//
//        })
//    }
}


struct ShopList_Previews: PreviewProvider {
    static var previews: some View {
        ShopList()
    }
}
