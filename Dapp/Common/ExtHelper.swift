//
//  Extensions.swift
//  Dapp
//
//  Created by Imthath M on 08/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import Foundation
import SwiftUI

extension Int {
    
    func isSimilar(to other: Int) -> Bool {
        String(other).contains(String(self))
    }
}

extension Notification.Name {
    static var goHome: Notification.Name { Notification.Name("goHome") }
    static var showPlaces: Notification.Name { Notification.Name("showPlaces")}
}

extension Color {
    static func primaryBG(for scheme: ColorScheme) -> Color {
        switch scheme {
        case .dark:
            return .black
        case .light:
            return .white
        }
    }
}

extension UIApplication {
    
    static func dismissKeyboard() {
        let keyWindow = shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .map({$0 as? UIWindowScene})
                .compactMap({$0})
                .first?.windows
                .filter({$0.isKeyWindow}).first
        keyWindow?.endEditing(true)
    }
}
