//
//  ItemList.swift
//  Dapp
//
//  Created by Imthath M on 19/01/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI
import CoreData
import MINetworkKit
import SDComponents

struct ItemList: View {
    
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    @ObservedObject var order: Order = Order.shared
    @State var cartDetail: SDCartDetail?    
    
    @State var showsAlert = false
    @State var showsLoader = false
    
    @FetchRequest var storeItems: FetchedResults<CDItem>
    @FetchRequest var localShops: FetchedResults<CDShop>
    
    let getItems = GetObject<[SDItem]>(network: Network.shared)
    let addItems = GetObject<SDCartDetail>(network: Network.shared)
    
    var isOrderPlaced = false
    
    var place: SDPlace
    
    
    init(shop: CDShop, place: SDPlace) {
        let shopPredicate = NSPredicate(format: "shopID = %@", shop.id)
        let availablePredicate = NSPredicate(format: "available = true")
        
        self._storeItems = FetchRequest(
            entity: CDItem.entity(),
            sortDescriptors: [NSSortDescriptor(keyPath: \CDItem.modifiedTime, ascending: false)],
            predicate: NSCompoundPredicate(andPredicateWithSubpredicates: [shopPredicate, availablePredicate]),
            animation: nil
        )
        
        self._localShops = FetchRequest(entity: CDShop.entity(), sortDescriptors: [],
                                  predicate: NSPredicate(format: "id = %@", place.shopID),
                                  animation: nil)
        
        self.place = place
        order.empty()
    }
    
    var body: some View {
        VStack {
            getItemsList(in: storeItems.map({ $0 }).groupedByCategories)
            Spacer()
            NavigationLink(destination: CartView(place: place, cartDetail: $cartDetail),
                           label: getOrderStatus)
                .disabled(isCartEmpty)
        }
        .navigationBarTitle("Menu")
        .navigationBarItems(leading: imageButton(withName: SDIcon.back, andAction: checkOrderPlaced),
                            trailing: imageButton(withName: SDIcon.reload, andAction: updateItems))
            .onAppear(perform: updateItems)
            .alert(isPresented: $showsAlert, content: getAlert)
    }
    
    // MARK:- Sub Views
    
    func getItemsList(in categories: [CategoryItems]) -> some View {
        Loader(isVisible: $showsLoader) {
            List {
                ForEach(categories) { category in
                    Section(header: Text("\(category.id)")) {
                        self.getItemList(category.items)
                    }
                }
            }
            .listStyle(GroupedListStyle())
//            .environment(\.horizontalSizeClass, .regular)
        }
        
        
    }
    
    func getItemList(_ items: [CDItem]) -> some View {
        ForEach(items) { item in
            ItemRow(item: item)
        }
    }
    
    var navBarButtons: some View {
        reloadButton
    }
    
    var reloadButton: some View {
        Button(action: updateItems, label: {
            Image(systemName: SDIcon.reload)
        })
    }
    
    private func getAlert() -> Alert {
        Alert(title: Text("Cancel Order ?"),
//              primaryButton: Alert.Button.default(Text("Yes, Cancel"), action: dismissView),
//              secondaryButton: Alert.Button.default(Text("No, Contiue"), action: dismissAlert))
              primaryButton: Alert.Button.default(Text("No, Contiue"), action: dismissAlert),
              secondaryButton: Alert.Button.default(Text("Yes, Cancel"), action: dismissView))
    }
    
    func getOrderStatus() -> some View {
        OrderStatusView(place: place, cartDetail: $cartDetail, isPlacingOrder: $showsLoader)
    }

    // MARK:- Helpers
    
    private var isCartEmpty: Bool { order.items.isEmpty && cartDetail.isNil }
    
    private func updateItems() {
        guard let shop = localShops.first else { return }
        
        if shop.itemsSyncedTime == 0 {
            //show loader
        }
        
        getItems.perform(Request.getItems(shop: shop)) { result in
            
            switch result {
            case .success(let items):
                items.forEach { $0.save() }
                shop.itemsSyncedTime = Int64(Date().timeIntervalSince1970)
                Cache.coreData.context.update()
            case .failure(let error):
                "\(error)".log()
            }
        }
    }
    
    private func checkOrderPlaced() {
        if order.items.count > 0,
            !isOrderPlaced {
            showsAlert = true
        } else {
            dismissView()
        }
    }
    
    private func dismissView() {
        order.empty()
        self.presentationMode.wrappedValue.dismiss()
    }
    
    private func dismissAlert() {
        self.showsAlert = false
    }
}

extension CDItem: Identifiable { }

struct ItemList_Previews: PreviewProvider {
    static var previews: some View {
        ItemList(shop: PreviewModel.shop, place: PreviewModel.place)
    }
}

extension Array where Element == CDItem {
    
    var groupedByCategories: [CategoryItems] {
        var result = [CategoryItems]()
        let dict = Dictionary.init(grouping: self, by: { $0.categoryID })
        
        for (key, value) in dict.sorted(by: { $0.key > $1.key}) {
            result.append(CategoryItems(id: key, items: value))
        }
        
        return result
    }
}

struct CategoryItems: Identifiable {
    let id: Int64
    let items: [CDItem]
}
