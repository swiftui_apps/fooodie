//
//  DataManager.swift
//  Dapp
//
//  Created by Imthath M on 20/01/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import Foundation
import SDComponents

class DataManager {
    static func save(_ shops: [SDShop]) {
        shops.forEach { $0.save() }
        Cache.coreData.context.update()
    }
    
    static func save(_ items: [SDItem]) {
        items.forEach { $0.save() }
        Cache.coreData.context.update()
    }
}

extension SDShop {
    
    func save() {
        let shop = CDShop(context: Cache.coreData.context)
        shop.id = self.id
        shop.name = self.name
        shop.createdTime = Int64(self.createdTime)
        shop.modifiedTime = Int64(self.modifiedTime ?? self.createdTime)
    }
}

extension SDItem {
    
    func save() {
        let item = CDItem(context: Cache.coreData.context)
        item.id = self.id
        item.shopID = self.shopID
        item.categoryID = self.categoryID
        item.name = self.name
        item.price = self.price
        item.imageURL = self.imageURL
        item.code = self.code
        item.createdTime = self.createdTime
        item.modifiedTime = self.modifiedTime ?? 0
        item.available = self.available
    }
}
