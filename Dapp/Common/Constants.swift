//
//  Constants.swift
//  Dapp
//
//  Created by Imthath M on 18/01/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SDComponents

//class Preference: ObservableObject {
//    
//    private init() { }
//    
//    public static let shared = Preference()
//    
//    @Published var isNavigationActive = false 
//}

class Order: ObservableObject {
    
    private init() { }
    
    internal static var shared: Order = Order()
    
    // IMPROVEMENT: use a struct as a published property and see if count changes
    // are reflected directly in the view
    @Published var items: [CDItem] = []
    
    // NOTE: other approaches of directly increasing count were discarded
    // because it would require an array of all items in the shop to be in memory always
    func add(_ item: CDItem) {
        if let index = items.firstIndex(where: {$0.id == item.id}) {
            items.remove(at: index)
        }
        
        items.append(item)
    }
    
    func remove(_ item: CDItem) {
        guard let index = items.firstIndex(where: {$0.id == item.id}) else { return }
        items.remove(at: index)
        
        if item.count != 0 {
            items.append(item)
        }
    }
    
    func empty() {
        for item in items {
            item.count = 0
        }
        items = []
    }
}

class Cache {
    static var jwt: String?
    static let coreData = SDCoreData(modelName: Constant.coreDataModel)
}

class Constant {
    //    static var baseURL: String { "https://sd-shop-service.herokuapp.com/api" }
//    static var baseURL: String { "http://192.168.43.160:8080/api" } //server as Asif's machine
//    static var baseURL: String { "http://192.168.43.108:8080/api" } // my mac mini server
        static var baseURL: String { "http://localhost:8080/api" }
    
    static var coreDataModel: String { "Dapp" }
    static var shopKey: String { "shop" }
}

class StaticStore {
    static var standard: UserDefaults { UserDefaults.standard }
    
    static var lastShopSynced: UInt64 {
        get {
            guard let result = standard.object(forKey: "lastShopSynced") as? UInt64 else {
                return 0
            }
            
            return result
        }
        
        set {
            standard.set(newValue, forKey: "lastShopSynced")
        }
    }
}

class Store: ObservableObject {
    @Published var shop: CDShop?
    
}
