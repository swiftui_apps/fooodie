//
//  CartView.swift
//  Dapp
//
//  Created by Imthathullah M on 26/01/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI
import SDComponents

struct CartView: View {
    var place: SDPlace
    @State var isLoading = false
    @Binding var cartDetail: SDCartDetail?
    @ObservedObject var order: Order = Order.shared
    
    
    var body: some View {
        stackView
        .navigationBarTitle("Your Items")
        .listStyle(GroupedListStyle())
        .environment(\.horizontalSizeClass, .regular)
    }
    
    private var stackView: some View {
        VStack {
            Loader(isVisible: $isLoading) {
                self.listView
                Spacer()
            }
            
            if !isCartEmpty {
                OrderStatusView(place: place, cartDetail: $cartDetail, isPlacingOrder: $isLoading)
            }
        }
    }
    
    private var listView: some View {
        List {
            if !order.items.isEmpty {
                pendingSection
            }
            
            if !cartDetail.isNil {
                orderedSection
            }
        }
    }
    
    private var pendingSection: some View {
        Section(header: Text("Pending to order"), content: {
            ForEach(order.items.sorted(by: { $0.id > $1.id}), id: \.id) { item in
                ItemRow(item: item)
            }
            //                .onDelete(perform: deleteItems)
        })
    }
    
    private var orderedSection: some View {
        Section(header: Text("Ordered Items")) {
            ForEach(cartDetail!.items, id: \.id) { item in
                HStack {
                    Text(item.item.name)
                    Spacer()
                    Text("\(item.count)")
                    self.getDeliveredIcon(basedOn: item.delivered)
                }.padding([.leading, .trailing], .small)
            }
        }
    }
    
    private func getDeliveredIcon(basedOn isDelivered: Bool) -> some View {
        if isDelivered {
            return Image(systemName: SDIcon.roundedCheck).foregroundColor(.green)
        }
        
        return Image(systemName: SDIcon.more).foregroundColor(.yellow)
    }
    
    private var isCartEmpty: Bool { order.items.isEmpty && cartDetail.isNil }
    
//    private func deleteItems(at offsets: IndexSet) {
//        for index in offsets {
//            order.items[index].count = 0
//        }
//        order.items.remove(atOffsets: offsets)
//    }
}

struct Cart_Previews: PreviewProvider {
    
    static var previews: some View {
        CartView(place: PreviewModel.place, cartDetail: .constant(PreviewModel.cartDetail))
    }
}

