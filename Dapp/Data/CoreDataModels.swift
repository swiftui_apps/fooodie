//
//  CDShop+CoreDataProperties.swift
//  
//
//  Created by Imthath M on 18/01/20.
//
//

import Foundation
import CoreData

@objc(CDShop)
public class CDShop: NSManagedObject { 

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDShop> {
        return NSFetchRequest<CDShop>(entityName: "CDShop")
    }

    @NSManaged public var id: String
    @NSManaged public var name: String
    @NSManaged public var isFavourite: Bool
    @NSManaged public var createdTime: Int64
    @NSManaged public var modifiedTime: Int64
    @NSManaged public var itemsSyncedTime: Int64
}

@objc(CDItem)
public class CDItem: NSManagedObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CDItem> {
        return NSFetchRequest<CDItem>(entityName: "CDItem")
    }

    @NSManaged public var id: Int64
    @NSManaged public var shopID: String
    @NSManaged public var categoryID: Int64
    @NSManaged public var name: String
    @NSManaged public var price: Double
    @NSManaged public var imageURL: String?
    @NSManaged public var code: String?
    @NSManaged public var createdTime: Int64
    @NSManaged public var modifiedTime: Int64
    @NSManaged public var available: Bool

    var count = 0
    var totalPrice: Double { price * Double(count) }
}
