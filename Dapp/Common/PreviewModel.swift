//
//  PreviewModel.swift
//  Dapp
//
//  Created by Imthath M on 06/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import Foundation
import SDComponents

#if DEBUG

class PreviewModel {
    
    static var userID: String { "cc639648-3917-464c-ab7b-1b565490757f" }
    static var itemNames = ["Briyani", "Pizza", "Sphagtti", "Dates and Apple Shake", "Meat balls"]
    
    static var user: SDUser {
        return SDUser(name: "guest", imageURL: nil)
    }
    
    static var shop: CDShop {
        let shop = CDShop(context: Cache.coreData.context)
        shop.id = "6bfad03b-3684-42af-9b16-81f852eb4ee1"
        return shop
    }
    
    static var place: SDPlace {
        return SDPlace(id: Int64.random(in: 1...100), shopID: shop.id,
                       type: ["TABLE", "ROOM"].randomElement()!,
                       number: Int.random(in: 1...60),
                       capacity: Int.random(in: 1...10),
                       available: Bool.random())
    }
    
    static let places: [SDPlace] = {
        var result = [SDPlace]()
        for id in 0...20 {
            result.append(place)
        }
        return result
    }()
    
    static var localItem: CDItem {
        let item = CDItem(context: Cache.coreData.context)
        item.id = Int64.random(in: 100...1000)
        item.name = itemNames.randomElement()!
        item.price = Double.random(in: 100...500)
        item.categoryID = Int64([1,2,3,4,5].randomElement()!)
        return item
    }
    
    static var localItems: [CDItem] {
        var result = [CDItem]()
        for _ in 0...20 {
            result.append(localItem)
        }
        return result
    }
    
    static var networkItem: SDItem {
        return SDItem(id: Int64.random(in: 100...1000),
                      shopID: shop.id, categoryID: Int64.random(in: 1...3),
                      name: itemNames.randomElement()!,
                      price: Double(Int.random(in: 100...500)),
                      imageURL: nil, code: nil, createdTime: 10,
                      modifiedTime: nil, available: true)
    }
    
    static var networkItems: [SDItem] {
        var result = [SDItem]()
        for _ in 0...20 {
            result.append(networkItem)
        }
        return result
    }
    
    static var cartItem: SDCartItem {
        return SDCartItem(id: Int64.random(in: 100...1000), item: networkItem,
                          count: Int.random(in: 1...10), delivered: Bool.random())
    }
    
    static var cartItems: [SDCartItem] {
        var result = [SDCartItem]()
        for _ in 0...7 {
            result.append(cartItem)
        }
        return result
    }
    
    static var cart: SDCart {
        return SDCart(id: Int64.random(in: 100...1000), shopID: shop.id, placeID: place.id, orderedBy: "USER", userID: userID, status: "NOT_DELIVERED")
    }
    
    static var cartDetail: SDCartDetail {
        return SDCartDetail(cart: cart, items: cartItems, user: user, place: place)
    }
}

#endif
