//
//  CheckOutView.swift
//  Dapp
//
//  Created by Imthath M on 07/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI

struct CheckOutView: View {
    
    let price: Double
    
    var body: some View {
        VStack {
            Text("placeholder for payments view").foregroundColor(.secondary)
            Spacer()
            VStack {
                Text("Your bill is ").padding()
                Text("₹\(price, specifier: "%.2f")").font(.title)
            }.layoutPriority(2)
            Spacer()
            Button(action: goHome, label: {
                Text("Done ✅")
                    .font(.title)
                    .foregroundColor(.white)
            })
            .padding()
            .background(Theme.buttonBackground)
                .cornerRadius(.small)
            Spacer()
        }
        
    }
    
    func goHome() {
        NotificationCenter.default.post(Notification(name: .goHome))
    }
}

struct CheckOutView_Previews: PreviewProvider {
    static var previews: some View {
        CheckOutView(price: 250)
    }
}
