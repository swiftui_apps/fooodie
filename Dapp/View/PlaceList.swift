//
//  PlaceList.swift
//  Dapp
//
//  Created by Imthathullah M on 01/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI
import MINetworkKit
import SDComponents

struct PlaceList: View {
    
    var shop: CDShop
    @State var places: [SDPlace] = []
    
    @State private var isLoading = false
    @State var tables: [SDPlace] = []
    @State var rooms: [SDPlace] = []
    @State var roomNumber: String = ""
    
    let getPlaces = GetObject<[SDPlace]>(network: Network.shared)
    
    var body: some View {
        let binding = Binding<String>(
            get: { self.roomNumber },
            set: { self.roomNumber = $0; self.filterPlaces() }
        )
        
        return Loader(isVisible: $isLoading) {
            VStack {
                if !self.places.isEmpty {
                    SearchView(searchText: binding)
                    self.listView
                } else {
                    EmptyView()
                }
            }
            
            
        }
        .onTapGesture(perform: UIApplication.dismissKeyboard)
        .navigationBarTitle("Choose a place")
        .navigationBarItems(trailing: imageButton(withName: SDIcon.reload, andAction: updatePlaces))
        .onAppear(perform: updatePlaces)
    }
    
    var listView: some View {
        let binding = Binding<String>(
            get: { self.roomNumber },
            set: { self.roomNumber = $0; self.filterPlaces() }
        )
        
        return List {
//            if !places.isEmpty {
                
//                StyleBox(title: "Search", placeholder: "Enter a table or room number to search", value: binding, keyboardType: .numberPad)
//            }
            
            if !tables.isEmpty {
                Section(header: Text("Tables")) {
                    ForEach(tables) { table in
                        PlaceRow(shop: self.shop, place: table)
                    }
                }
            }
            
            if !rooms.isEmpty {
                Section(header: Text("Rooms")) {
                    ForEach(rooms) { room in
                        PlaceRow(shop: self.shop, place: room)
                    }
                }
            }
        }
        .listStyle(GroupedListStyle())
        .gesture(DragGesture().onChanged({_ in UIApplication.dismissKeyboard() }))
    }
    
//    var scrollView: some View {
//        ScrollView {
//            
//            if !tables.isEmpty {
//                Divider()
//                Text("Tables")
//                Divider()
//                ForEach(tables) { place in
//                    PlaceCard(shop: self.shop, place: place)
//                }
//            }
//            
//            if !rooms.isEmpty {
//                Divider()
//                Text("Rooms")
//                Divider()
//                ForEach(rooms) { place in
//                    PlaceCard(shop: self.shop, place: place)
//                }
//            }
//            
//            
//        }
//    }
    
    func updatePlaces() {
        #if DEBUG
        self.places = PreviewModel.places
        self.tables = places.filter({ $0.type == "TABLE" })
        self.rooms = places.filter({ $0.type == "ROOM" })
        return
        #endif
        
        if tables.isEmpty,
            rooms.isEmpty {
            isLoading = true
        }
        
        getPlaces.perform(Request.getPlaces(shopID: shop.id)) { result in
            self.isLoading = false
            switch result {
            case .success(let places):
                self.places = places
                self.updateSections()
            case .failure(let error):
                "\(error)".log()
            }
        }
    }
    
    func updateSections() {
        tables = places.filter({ $0.type == "TABLE" })
        rooms = places.filter({ $0.type == "ROOM" })
    }
    
    func filterPlaces() {
        guard !roomNumber.isEmpty,
            let number = Int(roomNumber) else {
            updateSections()
            return
        }
        
        tables = places.filter({ number.isSimilar(to: $0.number) && $0.type == "TABLE" })
        rooms = places.filter({ number.isSimilar(to: $0.number) && $0.type == "ROOM" })
    }
}

struct PlaceRow: View {
    let shop: CDShop
    let place: SDPlace
    
    var body: some View {
        NavigationLink(destination: ItemList(shop: shop, place: place), label: {
            HStack {
                Text("\(place.type) - \(place.number)")
                Spacer()
                Text("Capacity - \(place.capacity)")
                    .padding(.trailing, .medium)
            }            
        })
    }
}

//struct PlaceCard: View {
//
//    let shop: CDShop
//    let place: SDPlace
//
//    var body: some View {
//        NavigationLink(destination: ItemList(shop: shop, place: place), label: {
//            HStack {
//                Text("\(place.type) - \(place.number)").foregroundColor(.primary)
//                Spacer()
//                Text("Capacity - \(place.capacity)").foregroundColor(.primary)
//                Image(systemName: SDIcon.forward).foregroundColor(.secondary)
//                    .padding(.leading, .small)
//            }
//            .padding(EdgeInsets(top: .medium, leading: .medium, bottom: .medium, trailing: .small))
//            .background(Color(UIColor.systemFill))
//            .cornerRadius(.small)
//            .padding(EdgeInsets(top: .zero, leading: .small, bottom: .zero, trailing: .small))
//
//        })
//
//    }
//}


struct PlaceList_Previews: PreviewProvider {    
    
    static var previews: some View {
        PlaceList(shop: PreviewModel.shop)//, places: PreviewModel.places)
    }
}


extension SDPlace: Identifiable { }
