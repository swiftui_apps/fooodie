//
//  OrderStatusView.swift
//  Dapp
//
//  Created by Imthath M on 07/02/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI
import SDComponents
import MINetworkKit

struct OrderStatusView: View {
    
    var place: SDPlace
    @ObservedObject var order = Order.shared
    @Binding var cartDetail: SDCartDetail?
    @Binding var isPlacingOrder: Bool
    
    @State var showCheckOutView = false
    
    let addItems = GetObject<SDCartDetail>(network: Network.shared)
    
    var body: some View {
        getCart()
    }
    
    private func getCart() -> some View {
            HStack {
                VStack(alignment: .leading) {
                    if !order.items.isEmpty {
                        BlackBoldText("Pending Order \(order.items.count) items of count \(order.items.map({ $0.count }).reduce(0, +))")
                    }
                    if !cartDetail.isNil {
                        BlackBoldText("Ordered \(cartDetail!.items.count) items of count \(cartDetail!.items.map({ $0.count }).reduce(0, +))")
                    }
                }
                
                Spacer()
                NavigationLink(destination: CheckOutView(price: price), isActive: $showCheckOutView) {
                   orderButton
                }//.disabled(canCheckOut)
            }
            .padding(.small)
            .background(cartBackground)
    //        .cornerRadius(.medium)
    //        .padding(.small)
            .toggleVisibility(basedOn: !isCartEmpty)
            .frame(height: !isCartEmpty ? 100 : .zero)
        }
        
        var cartBackground: some View {
            Color.orange.opacity(0.75)
        }
    
    private var orderButton: some View {
        Button(action: orderOrCheckOut, label: {
            VStack {
                Text(canCheckOut ? "Check Out" : "Order")
                    .font(canCheckOut ? .headline : .title)
                    .foregroundColor(.white)
                    .padding(EdgeInsets(top: .small, leading: .large, bottom: .zero, trailing: .large))
                Text("₹ " + String(format: "%.2f", price))
                    .font(.footnote)
                    .foregroundColor(.white)
                    .padding(.small)
            }
            .background(Theme.buttonBackground)
            .cornerRadius(.small)
        })
    }
    
    private var isCartEmpty: Bool { order.items.isEmpty && cartDetail.isNil }
    
    private var canCheckOut: Bool { !cartDetail.isNil && order.items.isEmpty }
    
    private var pendingPrice: Double {
        order.items.map({ $0.totalPrice }).reduce(0, +)
    }
    
    private var orderedPrice: Double {
        guard let cartDetail = self.cartDetail else { return 0 }
        
        return cartDetail.items.map({ $0.item.price * Double($0.count)}).reduce(0, +)
    }
    
    private var price: Double {
        pendingPrice + orderedPrice
    }
    
    private func orderOrCheckOut() {
        if canCheckOut {
            checkOut()
        } else {
            orderItems()
        }
    }
    
    private func orderItems() {
        guard order.items.count > 0 else { return }
        isPlacingOrder = true
        var result = [String: Int]()
        for item in order.items {
            result.updateValue(item.count, forKey: String(item.id))
        }
        
        let request = AddItemsRequest(shopID: place.shopID, placeID: place.id, cartID: cartDetail?.cart.id, items: result)
        
        addItems.perform(Request.addToCart(request)) { result in
            switch result {
            case .success(let cartDetail):
                self.cartDetail = cartDetail
                self.order.empty()
            case .failure(let error):
                "\(error)".log()
            }
            self.isPlacingOrder = false
        }
    }
    
    private func checkOut() {
        showCheckOutView = true
    }
}

struct OrderStatusView_Previews: PreviewProvider {
    static var previews: some View {
        OrderStatusView(place: PreviewModel.place, cartDetail: .constant(PreviewModel.cartDetail), isPlacingOrder: .constant(false))
    }
}
