//
//  ItemRow.swift
//  Dapp
//
//  Created by Imthath M on 25/01/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI
import SDComponents

struct ItemRow: View {
    
    @ObservedObject var order: Order = Order.shared
    
    var item: CDItem
    
    var count: Int { order.items.filter({ $0.id == item.id}).first?.count ?? 0 }
    
    var body: some View {
        HStack {
            itemNameAndPrice
            Spacer()
            itemCount
        }
            // to disable the row selection
            .buttonStyle(PlainButtonStyle())
    }
    
    func addItem() {
        item.count += 1
        order.add(item)
    }
    
    func removeItem() {
        guard item.count > 0 else { return }

        item.count -= 1
        order.remove(item)
    }
    
    var itemNameAndPrice: some View {
        VStack(alignment: .leading) {
            Text(item.name)
                .font(.body)
                .padding(.small)
            Text(String(format: "%.2f", item.price))
                .font(.footnote)
                .foregroundColor(.secondary)
                .padding([.leading, .bottom], .small)
        }
    }
    
    var itemCount: some View {
        HStack {
            Text(String(count))
                .toggleVisibility(basedOn: count > 0)
                .padding()
            Button(action: removeItem, label: {
                Image(systemName: SDIcon.minus)
                    .foregroundColor(.blue)
            }).toggleVisibility(basedOn: count > 0)
            Button(action: addItem, label: {
                Image(systemName: SDIcon.plus)
                    .foregroundColor(.blue)
            }).padding()
        }
    }
}

struct ItemRow_Previews: PreviewProvider {
    static var previews: some View {
        ItemRow(item: PreviewModel.localItem)
    }
}
