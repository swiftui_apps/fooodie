//
//  NetworkManager.swift
//  Dapp
//
//  Created by Imthath M on 18/01/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import MINetworkKit
import SDComponents

class Network: MINetworkable {
    
    private init() {
        SDCache.baseURL = Constant.baseURL
    }
    
    public static let shared = Network()
    
    func formURLRequest(basedOn request: MIRequest, onCompletion handler: @escaping (Result<URLRequest, MINetworkError>) -> Void) {
        
        guard let myRequest = request as? Request else { return }
        if !myRequest.isAuthenticationRequired,
            let request = getURLrequest(from: request) {
            handler(.success(request))
            return
        }
        
        login { isSuccess in
            guard isSuccess,
                let request = self.getURLrequest(from: request) else {
                    handler(.failure(.accessDenied))
                    return
            }
            
            handler(.success(request))
        }
        
    }
    
    func login(onCompletion handler: @escaping (Bool) -> Void) {
        request(SDRequest.login(identifier: "firstGuest", password: "testing"),
                returns: [SDauthToken]()) { result in
                    switch result {
                    case .success(let token):
                        Cache.jwt = token.jwt
                        handler(true)
                    case .failure(let error):
                        "\(error)".log()
                        handler(false)
                    }
        }
    }
    
    internal func request<AnyDecodable: Decodable>(_ myRequest: MIRequest, returns responseAs: [AnyDecodable],
                                                   onCompletion handler: @escaping (Result<AnyDecodable, MINetworkError>) -> Void) {
        guard let request = getURLrequest(from: myRequest) else {
            handler(.failure(.badURL))
            return
        }
        
        session.dataTask(with: request) { data, response, error in
            handler(self.parse(data, response, error))
        }.resume()
        
    }
}

struct AddItemsRequest {
    let shopID: String
    let placeID: Int64
    let cartID: Int64?
    let items: [String: Int]
}

enum Request: MIRequest {
    case getShops
    case getPlaces(shopID: String)
    case getItems(shop: CDShop)
    case addToCart(AddItemsRequest)
    
    var urlString: String {
        return Constant.baseURL + path
    }
    
    var path: String {
        switch self {
        case .getShops:
            return "/shop"
        case .getPlaces(let shopID):
            return "/shop/\(shopID)/place"
        case .getItems(let shop):
            return "/shop/\(shop.id)/item"
        case .addToCart(let request):
            return "/shop/\(request.shopID)/place/\(request.placeID)"
        }
    }
    
    var method: MINetworkMethod {
        switch self {
        case .getShops, .getPlaces, .getItems:
            return .get
        case .addToCart:
            return .post
        }
    }
    
    var params: [String : Any]? {
        switch self {
        case .getShops, .getPlaces:
            return nil
        case .getItems(let shop):
            return ["modifiedTime": shop.itemsSyncedTime * 1000]
        case .addToCart(let request):
            if let cartID = request.cartID {
                return ["cartID": cartID]
            }
            return nil
        }
    }
    
    var headers: [String : String]? {
        
                switch self {
                case .getShops, .getPlaces, .getItems:
                    return nil
                case .addToCart:
                    if isAuthenticationRequired,
                        let jwt = Cache.jwt {
                        return ["Authorization": "Bearer \(jwt)", SDHeader.contentTypeKey: SDHeader.contentTypeJSON]
                    }
                    
                    return SDHeader.json
                }
    }
    
    var body: Data? {
        switch self {
        case .getShops, .getPlaces, .getItems:
            return nil
        case .addToCart(let request):
            return request.items.jsonData
        }
    }
    
    var isAuthenticationRequired: Bool {
        switch self {
        case .getShops, .getPlaces, .getItems:
            return false
        case .addToCart:
            return true
        }
    }
    
}
