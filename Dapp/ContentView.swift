//
//  ContentView.swift
//  Dapp
//
//  Created by Imthath M on 18/01/20.
//  Copyright © 2020 SkyDevz. All rights reserved.
//

import SwiftUI
import UIKit

struct ContentView: View {
    var body: some View {
        NavigationView {
            ShopList()
//            PlaceList(shop: PreviewModel.shop)
//            ItemList(shop: PreviewModel.shop, place: PreviewModel.place)
//            CartView(place: PreviewModel.place, cartDetail: .constant(PreviewModel.cartDetail))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
//        ContentView()
        StyleBox(title: "Email or user name", placeholder: "", value: .constant("imthath.m@hotmail.com"), keyboardType: .emailAddress)
    }
}

class Theme {
    static var buttonBackground: some View { Color.blue }
}

struct SearchView: View {
    @Binding var searchText: String
    @State var showCancelButton: Bool = false

    var body: some View {
        // Search view
        HStack {
            HStack {
                Image(systemName: "magnifyingglass")

                TextField("search", text: $searchText, onEditingChanged: { isEditing in
                    self.showCancelButton = true
                }, onCommit: {
                    print("onCommit")
                }).foregroundColor(.primary)

                Button(action: {
                    self.searchText = ""
                }) {
                    Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                }
            }
            .padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))
            .foregroundColor(.secondary)
            .background(Color(.secondarySystemBackground))
            .cornerRadius(10.0)

            if showCancelButton  {
                Button("Cancel") {
                        UIApplication.dismissKeyboard() // this must be placed before the other commands here
                        self.searchText = ""
                        self.showCancelButton = false
                }
                .foregroundColor(Color(.systemBlue))
            }
        }
        .padding(.horizontal)
        .navigationBarHidden(showCancelButton)// TODO: ANIMATE.animation(nil)
    }
}
struct BlackBoldText: View {
    let text: String
    
    init(_ text: String) {
        self.text = text
    }
    
    var body: some View {
        Text(text)
            .foregroundColor(.black)
            .bold()
    }
}

struct StyleBox: View {
    @Environment(\.colorScheme) var theme
    
    var title: String
    var placeholder: String
    
    @Binding var value: String
    
    var keyboardType = UIKeyboardType.default
    var boxColor = Color.secondary
    var backGroundColor = BackgroundColor.primary
    
    var body: some View {
        ZStack(alignment: .topLeading) {
            TextField(placeholder, text: $value)
                .keyboardType(keyboardType)
                .padding(EdgeInsets(top: .medium, leading: .medium, bottom: .medium, trailing: .small))
                .overlay(RoundedRectangle(cornerRadius: .small * 0.5).stroke(Color.secondary))
            Text(title)
                .padding([.leading, .trailing], 5)
                .background(background)
                .foregroundColor(boxColor).font(.footnote)
                .offset(x: .small, y: -.small)
        }.padding(EdgeInsets(top: .medium, leading: .small, bottom: .small, trailing: .small))
        .background(background)
        
    }
    
    var background: Color {
        switch backGroundColor {
        case .primary:
            return Color.primaryBG(for: theme)
        case .secondary:
            return Color(UIColor.secondarySystemBackground)
        case .customColor(let color):
            return color
//        case .customImage(let image):
//            return image
        }
    }
    enum BackgroundColor {
        case primary
        case secondary
        case customColor(Color)
//        case customImage(Image)
    }
}

class SDNavigationController: UINavigationController {
    
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        NotificationCenter.default.addObserver(self, selector: #selector(showPlaces), name: .showPlaces, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(goHome), name: .goHome, object: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func showPlaces(notification: Notification) {
        
        guard let dict = notification.userInfo,
            let shop = dict[Constant.shopKey] as? CDShop else {
            "unable to navigate to shop. ".log()
            return
        }
        
        let contentView = PlaceList(shop: shop)
                            .environment(\.managedObjectContext, Cache.coreData.context)
        let viewController = UIHostingController(rootView: contentView)
        pushViewController(viewController, animated: true)
    }
    
    @objc func goHome(notification: Notification) {
        self.popToRootViewController(animated: true)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
